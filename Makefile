.PHONY:  kubernetes/app.production.yaml,  kubernetes/app.staging.yaml, unit_test, release

unit_test:
	@echo "+++ Unit tests +++"
override printRow = @printf "%+22s = %-s\n" $1 $2

override M4_OPTS = \
	--define "m4ReleaseImage=$(DOCKER_IMAGE)" \
	--define "m4CommitSHA=$(CI_COMMIT_SHA)" \
	--define "m4VersionTag=$(VERSION_TAG)" 

kubernetes/app.staging.yaml: kubernetes/app.staging.m4.yaml  Makefile
	@echo " + + + Build Kubernetes app staging yml + + + "
	@m4 $(M4_OPTS) kubernetes/app.staging.m4.yaml > kubernetes/app.staging.yaml

kubernetes/app.production.yaml: kubernetes/app.production.m4.yaml  Makefile
	@echo " + + + Build Kubernetes production app yml + + + "
	@m4 $(M4_OPTS) kubernetes/app.production.m4.yaml > kubernetes/app.production.yaml

clean:
	@rm -rf kubernetes/app.production.yaml kubernetes/app.staging.yaml

release:
	$(if $(VERSION_TAG),,$(error: set project version string on VERSION_TAG, when calling this task))
	@echo " + + + Set next version: $(VERSION_TAG) + + + "
	@make clean
	@make kubernetes/app.production.yaml
	@echo " + + + Push tags to repository + + + "
	@git add .
	@git commit -m "Changes for next release $(VERSION_TAG)"
	@git tag -s $(VERSION_TAG) -m "Next release $(VERSION_TAG)"
	@git push --tags origin master


docker_login:
	@echo " + + + Login into registry: $(REGISTRY) +  +  + "
	@docker login -p$(REGISTRY_PASSWORD) -u$(REGISTRY_USER) $(REGISTRY)

docker_logout:
	@echo " + + + Logout from registry: $(REGISTRY) +  +  + "
	@docker logout $(REGISTRY)
