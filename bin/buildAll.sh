#!/usr/bin/env sh

set -ex

VERSION_TAG=${VERSION_TAG-'7985b465cccb5c9a0649bc57181cfc38fb358d19'}
DOCKER_IMAGE=${DOCKER_IMAGE-'local'}

# build and tag intermediate images and application image
docker build . --target composer -t $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/composer:latest
docker build . --target node-admin -t $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest
docker build . --target node-website -t $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest
docker build . --target server -t $DOCKER_IMAGE/server:latest \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/server:latest 
docker build . --target production -t $DOCKER_IMAGE/production:$VERSION_TAG \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/server:latest
