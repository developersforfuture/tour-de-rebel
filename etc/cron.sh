#!/bin/bash

set -e

# pass the (kubernetes) environment variables to the cronjobs
printenv | sed 's/^\([a-zA-Z0-9_]*\)=\(.*\)$/\1="\2"/g' > /tmp/out
cat /etc/crontab >> /tmp/out
mv /tmp/out /etc/crontab
crontab /etc/crontab

cron -f
