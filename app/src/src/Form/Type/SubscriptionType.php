<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class SubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailAddress', EmailType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.emailAddress'], 'required' => true, 'help' => 'form.help.emailAddress'])
            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'label' => true,
                    'required' => true,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ]
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'form.label.submit']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
