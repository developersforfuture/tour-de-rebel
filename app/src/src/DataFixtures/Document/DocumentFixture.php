<?php

namespace App\DataFixtures\Document;

use App\DataFixtures\ORM\AppFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use RuntimeException;
use Sulu\Bundle\DocumentManagerBundle\DataFixtures\DocumentFixtureInterface;
use League\Csv\Reader;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Document\RedirectType;
use Sulu\Component\Content\Document\WorkflowStage;
use Sulu\Component\DocumentManager\DocumentManager;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\DocumentManager\Exception\MetadataNotFoundException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sulu\Component\PHPCR\PathCleanup;

class DocumentFixture implements DocumentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function getOrder(): int
    {
        return 10;
    }

    private function getEntityManager(): EntityManagerInterface
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->entityManager;
    }


    private function getMediaId(string $name): int
    {
        try {
            $id = $this->getEntityManager()->createQueryBuilder()
                ->from(Media::class, 'media')
                ->select('media.id')
                ->innerJoin('media.files', 'file')
                ->innerJoin('file.fileVersions', 'fileVersion')
                ->where('fileVersion.name = :name')
                ->setMaxResults(1)
                ->setParameter('name', $name)
                ->getQuery()->getSingleScalarResult();

            return (int) $id;
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException(sprintf('Too many images with the name "%s" found.', $name), 0, $e);
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @throws DocumentManagerException
     */
    private function loadHomepage(DocumentManager $documentManager): void
    {
        /** @var HomeDocument $homeDocument */
        $homeDocument = $documentManager->find('/cmf/tourdeplanet/contents', AppFixtures::LOCALE_DE);
        $blocks = [];
        $commonGridValues = [
            'type' => 'text_block',
            'collg' => 6,
            'colmd' => 6,
            'col' => 12,
            'space' => 4,
        ];
        $textBlocks = [
            '<p>Mit der Tour de Planet kannst du Teil der weltgrößten Klima-, Friedens- und Gerechtigkeitsbewegung werden und auf die Verhandlungen beim 26. UN-Klimagipfel in Glasgow Einfluss nehmen!</p>',
            '<p>Wir wollen mit den unterschiedlichsten Menschen weltweit in Bewegung kommen und als Gemeinschaft ein Zeichen setzen. Komm mit und schließe dich der Bewegung an!</p>',
            '<p>Bei der Tour de Planet kann jeder mitmachen, egal wie alt du bist, egal wie fit du bist, egal wie viel Zeit du hast, egal wo du wohnst. Schnapp dir dein Fahrrad, deinen Tretroller, dein Pferd, deine Rollschuhe oder zieh dir die Wanderstiefel an. Gemeinsam wollen wir emissionsfreie Kilometer sammeln. In einer großen Gruppe entlang der fest stehenden Routen oder auf einer eigenen Route, allein oder mit Freunden & Familie. Du entscheidest, welche Strecke du zurücklegen kannst und möchtest. Jeder Kilometer zählt - Fang an zu sammeln!</p>',
            [
                'type' => 'newsletter_block',
                'title' => 'Abboniere unseren Newsletter und bleibe auf dem aktuellsten Stand!',
                'form_label' => 'E-Mail-Adresse',
                'button_label' => 'Newsletter abonieren',
            ],
            [
                'type' => 'title_block',
                'close_row' => true,
                'size' => 2,
                'title' => 'Was erwartet dich?',
                'collg' => 12,
                'colmd' => 12,
                'col' => 12,
                'space' => 4,
            ],
            '<p><b>Vernetzung:</b></br> Begegnungen mit Menschen, die so empfinden wie du und auch die Welt verbessern wollen. Treffen mit verschiedenen Klima-, Friedens- und Gerechtigkeitsbewegungen.</p>',
            '<p><b>Gemeinschaft:</b></br>Eine Reisegemeinschaft aus Menschen, die das Glück im Kontakt mit Gleichgesinnten suchen, fern von Besitz und Konsum, die das gemeinsame und saubere Reisen in der Natur schätzen und die ein großes gemeinsames Ziel verbindet: Klimagerechtigkeit, soziale Gerechtigkeit und Frieden - Jetzt!</p>',
            '<p><b>Voneinander lernen:</b></br>Vermittlung der wissenschaftlichen Erkenntnisse zur Klimakrise und Fakten zu Missständen in der Welt an Menschen, die noch nicht so viel darüber wissen. Lernen von anderen Menschen, die an der Tour de Planet teilnehmen und von Menschen, denen wir unterwegs begegnen.</p>',
            '<p><b>Nachhaltigkeit:</b></br>Langsames, emissionsfreies Reisen im Einklang mit der Natur, mit oder ohne Camping. Reisen mit Verantwortungsbewusstsein füreinander, unsere Welt und unsere Zukunft.</p>',
            [
                'type' => 'title_block',
                'close_row' => true,
                'size' => 2,
                'title' => 'Was ist Die Tour de Planet genau?',
                'collg' => 12,
                'colmd' => 12,
                'col' => 12,
                'space' => 4,
            ],
            [
                'type' => 'text_block',
                'close_row' => true,
                'body' => '<p>Die Tour de Planet ist eine Bewegung, die sich für Klimagerechtigkeit, soziale Gerechtigkeit und Frieden einsetzt. Als eine sich bewegende Bewegung der Bewegungen verbindet sie Menschen aller Kontinente und alle schon bestehenden Klima- und Gerechtigkeitsbewegungen in einer Aktion. Die Tour de Planet ist eine Fahrrad-Tour/eine Wanderung/ein Wanderritt/etc. über tausende von Kilometern auf allen Kontinenten und hat das Ziel, unterwegs mit Menschen über Klimagerechtigkeit zu sprechen, von verschiedenen Menschen zu lernen, sowie die wissenschaftlichen Erkenntnisse zur Klimakrise und Ungerechtigkeiten in der Welt über Gespräche und verschiedene Aktionen weiterzutragen.</p>',
                'collg' => 12,
                'colmd' => 12,
                'col' => 12,
                'space' => 4,
            ],
            '<p>Das bestehende Engagement einzelner Gruppen oder Projekte für mehr Klimagerechtigkeit, soziale Gerechtigkeit und Frieden soll sichtbar und erlebbar gemacht werden. So wollen wir auf der Reise auch Orte und Aktionen besuchen, in denen oder durch die schon viel getan wird. Damit wollen wir auf alternative Handlungsoptionen aufmerksam machen und zu neuem Engagement ermutigen. Gleichzeitig soll aber auch aufgezeigt werden, an welchen Punkten noch intensive Bemühungen und politische Weichenstellungen nötig sind und wie wir darauf Einfluss nehmen können.</p>',
            '<p>Die  Summe der weltweit gesammelten Kilometer wird am 9. November beim 26. UN-Klimagipfel in Glasgow den Regierungschefs der beteiligten 193 Länder übergeben. So wollen wir ein Zeichen setzen und eine Politik fordern, die die wissenschaftlichen Erkenntnisse zur Klimakrise und alle damit verbundenen Ungerechtigkeiten endlich ernst nimmt und entsprechend handelt.</p>',
            '<p>Auf unserer Karte (Karte folgt...) kannst Du verschiedene Routen sehen, die in Glasgow enden. Kommst du nicht aus Europa, kann eine Hauptstadt oder ein bedeutungsvoller Ort zu auf deinem Kontinent symbolisch für Glasgow stehen und Ziel der Reise sein. Ab dem 01. April 2020 werden sich verschiedene Gruppen auf den Weg nach Glasgow/einem symbolischen Glasgow begeben und verschiedene Städte entlang der Routen durchfahren. Die Gruppen werden dort halten, sich regenerieren und aktiv sein. Im Kalender (Kalender folgt...) kannst du nachsehen, wann und wie lange die Tour de Planet in welcher Stadt sein wird und welche Aktionen vor Ort geplant sind. Dort kannst du dich einer Gruppe anschließen, dich mit den Menschen der verschiedenen Bewegungen vernetzen und an Aktionen teilnehmen. Unterwegs auf der Route werdet ihr auch in kleineren Städten und Dörfern mit Kommunalpolitikern sprechen, Menschen auf die Klimakrise und die Tour de Planet aufmerksam machen, Unterschriften sammeln und Fotos machen. Darüber hinaus motivierst du durch deine Teilnahme andere Menschen zum langsamen und klimafreundlichen Reisen in enger Verbindung zur Natur und in Gesellschaft interessanter und einflussreicher Menschen. Zudem bist du auch noch körperlich aktiv.</p>',
            '<p><b>Die Hauptroute ist für dich schwer erreichbar, du kannst zu den angegebenen Zeiten nicht zur Gruppe stoßen oder du möchtest deine ganz eigene Tour planen?</p>',
            '<p>Dann plane anhand unserer Karte (Karte folgt...) selbstverantwortlich Routen, um das Netz zu vergrößern. All die Nebenrouten können miteinander vernetzt und auf diese Weise mit Glasgow verbunden werden. Für deine eigene Route bist du selbst verantwortlich. Wir geben dir einen kleinen Leitfaden zur Hand, der dir zeigt, woran du denken solltest, damit deine eigene Tour de Planet reibungslos abläuft.</p>',
            '<p>Auf unserer Website wirst du ab dem 01.03.2020 noch mehr über die Tour de Planet erfahren und deine Tour planen. Wir würden uns freuen, wenn du Teil der Tour de Planet wirst und mit uns die Welt verbesserst!</p>',
        ];
        foreach ($textBlocks as $title => $body) {
            if (!is_array($body) && strlen($title) <= 3) {
                $blocks[] = array_merge(['body' => $body], $commonGridValues);
            } elseif (is_array($body)) {
                $blocks[] = $body;
            } else {
                $blocks[] = [
                    'type' => 'text_block',
                    'title' => $title,
                    'collg' => 12,
                    'colmd' => 12,
                    'col' => 12,
                    'space' => 4,
                ];
            }
        }
        $homeDocument->getStructure()->bind(
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => $homeDocument->getTitle(),
                'structureType' => 'homepage',
                'parent_path' => '/cmf/tourdeplanet/contents',
                'pagetitle' => 'Tour de Planet',
                'intro_title' => 'Was erwartet dich?',
                'intro_text' => '<p>Du sorgst dich um das Klima und den Frieden auf unserem Planeten und möchtest endlich aktiv werden?</p>',
                'blocks' => $blocks,
                'url' => '/',
                'seo' => [
                    'title' => 'Tour de Planet',
                    'description' => 'Tour de Planet is a moving movement of movements',
                ]
            ]
        );

        $documentManager->persist($homeDocument, AppFixtures::LOCALE_DE);
    }

    private function loadPages(DocumentManager $documentManager): array
    {
        $pages =  [
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Impressum',
                'structureType' => 'default',
                'url' => '/impressum',
                'parent_path' => '/cmf/tourdeplanet/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Tour de Rebel - Inprint',
                    'description' => 'Die Pflichtseite.',
                ],
                'article' => '
                    <div class="body-text clearfix">
                        <p><strong>Angaben gemäß § 5 TMG</strong><br>
                        Marc Schmitt-Weigand, Tiewinkel 8, 44319 Dortmund</p>
                        
                        <p><strong>Verantwortlich für den Inhalt nach § 55 Abs.2 RStV</strong><br>
                        Marc Schmitt-Weigand, Tiewinkel 8, 44319 Dortmund</p>
                        
                        <p><a href="/de/form/nimm-kontakt-zu-uns-auf">Kontakt</a></p>
                        
                        <p><strong>Haftung für Inhalte</strong><br>
                        Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den<br>
                        allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. Haftung für Links Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
                        
                        <p><strong>Urheberrecht</strong><br>
                        Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
                        
                        <p><strong>SocialMedia</strong></p>
                        
                        <p>Dieses Impressum gilt auch für die parents for future Social Media Angebote auf</p>
                        
                        <ul><
                            <li><a href="https://www.youtube.com/channel/UCjszvDT7ie05NlWdRV_nx4g?view_as=subscriber" target="_blank">Youtube</a></li>
                            <li><a href="https://twitter.com/tourdeplanet" target="_blank">Twitter @tourdeplanet</a></li>
                        </ul>
                    </div>'
            ],
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Datenschutz',
                'structureType' => 'default',
                'url' => '/datenschutz',
                'parent_path' => '/cmf/tourdeplanet/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Tour de Rebel - Dataprivacy',
                    'description' => 'Alles rund um den Datenschutz',
                ],
                'article' => '
                        <div class="body-text clearfix">
                        <p><strong>Datenschutz</strong></p>
            
                        <p>Wir als Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Ihre personenbezogenen Daten werden vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung behandelt.</p>
                        
                        <p>Die Nutzung unserer Website ist generell ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.</p>
                        
                        <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</p>
                        
                        <p><strong>Cookies</strong></p>
                        
                        <p>Internetseiten verwenden teilweise so genannte Cookies. Cookies richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert.<br>
                        Die meisten der von uns verwendeten Cookies sind so genannte „Session-Cookies“. Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim nächsten Besuch wiederzuerkennen.</p>
                        
                        <p>Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.</p>
                        
                        <p><strong>Server-Log-Files</strong></p>
                        
                        <p>Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log Files, die Ihr Browser automatisch an uns übermittelt.</p>
                        
                        <p>Dies sind:</p>
                        
                        <p>– Browsertyp und Browserversion<br>
                        – verwendetes Betriebssystem<br>
                        – Referrer URL<br>
                        – Hostname des zugreifenden Rechners<br>
                        – Uhrzeit der Serveranfrage</p>
                        
                        <p>Diese Daten sind nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden.</p>
                        
                        <p><strong>Recht auf Auskunft, Löschung, Sperrung</strong></p>
                        
                        <p>Sie haben jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.</p>
                        
                        <p><strong>Widerspruch Werbe-Mails</strong></p>
                        
                        <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter<br>
                        Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.</p>
                        
                        <p>Quelle: <a href="http://www.e-recht24.de">www.e-recht24.de</a></p>
                        
                        <p>Civist Petitionen Datenschutzrichtlinie</p>
                        
                        <p>Civist collects some Personal Data from its Users.</p>
                        
                        <p>Contact information</p>
                        
                        <p>Civist GmbH is the owner, controller and processor of dataCivist GmbHScharnhorststr. 24 10115 Berlin <a href="mailto:Germanyprivacy@civist.com">Germanyprivacy@civist.com</a></p>
                        
                        <p><strong>Account Registration</strong></p>
                        
                        <p>This is the personal data that is provided by you or collected by us to enable you to sign up for and use the Civist Service.Email Address, IP AddressSome of the personal data we will ask you to provide is required in order to create your account.You also have the option to provide us with some additional personal data in order to make your account more personalized.The exact personal data we will collect depends on the type of Civist Service plan you sign up for and whether or not you use a Third Party Service (as defined in the Terms of Service, such as WordPress) to sign up and use the Civist Service.If you use a Third Party Service to create an account, we will receive personal data via that Third Party Service but only when you have consented to that Third Party Service sharing your personal data with us.Role: ControllerRetention: until deletionSub-processor: Mailjet SAS</p>
                        
                        <p><strong>Fundraising Registration</strong></p>
                        
                        <p>This is the personal data that is provided by you or collected by us to enable you to sign up for and use the Civist Fundraising Service.First Name, Last Name, Date of Birth, Email Address, IBAN NumberSome of the personal data we will ask you to provide is required in order to create your fundraising account.You also have the option to provide us with some additional personal data in order to comply with a legal obligation under applicable law.The exact personal data we will collect depends on the type of Civist Fundraising Service plan you sign up for and whether or not you use a Third Party Service (as defined in the Terms of Service, such as Stripe) to sign up and use the Civist Fundraising Service.Role: Controller, ProcessorRetention: until deletionSub-processor: Mailjet SAS, MANGOPAY SA, Stripe, Inc.</p>
                        
                        <p>Civist – Petitionsplug-in</p>
      
            </div>'
            ]
        ];
        foreach ($pages as $pageData) {
            $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    private function getPathCleanup(): PathCleanup
    {
        if (null === $this->pathCleanup) {
            $this->pathCleanup = $this->container->get('sulu.content.path_cleaner');
        }

        return $this->pathCleanup;
    }


    /**
     * @param mixed[] $data
     *
     * @throws MetadataNotFoundException
     */
    private function createPage(DocumentManager $documentManager, array $data): BasePageDocument
    {
        $locale = isset($data['locale']) && $data['locale'] ? $data['locale'] : AppFixtures::LOCALE_DE;

        if (!isset($data['url'])) {
            $url = $this->getPathCleanup()->cleanup('/' . $data['title']);
            if (isset($data['parent_path'])) {
                $url = mb_substr($data['parent_path'], mb_strlen('/cmf/developerstourdeplanet/contents')) . $url;
            }

            $data['url'] = $url;
        }

        $extensionData = [
            'seo' => $data['seo'] ?? [],
            'excerpt' => $data['excerpt'] ?? [],
        ];

        unset($data['excerpt']);
        unset($data['seo']);

        /** @var PageDocument $pageDocument */
        $pageDocument = null;

        try {
            if (!isset($data['id']) || !$data['id']) {
                throw new Exception();
            }

            $pageDocument = $documentManager->find($data['id'], $locale);
        } catch (Exception $e) {
            $pageDocument = $documentManager->create('page');
        }

        $pageDocument->setNavigationContexts($data['navigationContexts'] ?? []);
        $pageDocument->setLocale($locale);
        $pageDocument->setTitle($data['title']);
        $pageDocument->setResourceSegment($data['url']);
        $pageDocument->setStructureType($data['structureType'] ?? 'default');
        $pageDocument->setWorkflowStage(WorkflowStage::PUBLISHED);
        $pageDocument->getStructure()->bind($data);
        $pageDocument->setAuthor(1);
        $pageDocument->setExtensionsData($extensionData);

        if (isset($data['redirect'])) {
            $pageDocument->setRedirectType(RedirectType::EXTERNAL);
            $pageDocument->setRedirectExternal($data['redirect']);
        }

        $documentManager->persist(
            $pageDocument,
            $locale,
            ['parent_path' => $data['parent_path'] ?? '/cmf/tourdeplanet/contents']
        );

        // Set dataSource to current page after persist as uuid is before not available
        if (isset($data['pages']['dataSource']) && '__CURRENT__' === $data['pages']['dataSource']) {
            $pageDocument->getStructure()->bind(
                [
                    'pages' => array_merge(
                        $data['pages'],
                        [
                            'dataSource' => $pageDocument->getUuid(),
                        ]
                    ),
                ]
            );

            $documentManager->persist(
                $pageDocument,
                $locale,
                ['parent_path' => $data['parent_path'] ?? '/cmf/tourdeplanet/contents']
            );
        }

        $documentManager->publish($pageDocument, $locale);

        return $pageDocument;
    }

    /**
     * @throws DocumentManagerException
     * @throws MetadataNotFoundException
     * @throws Exception
     */
    public function load(DocumentManager $documentManager): void
    {
        $this->loadHomepage($documentManager);
        $this->loadPages($documentManager);
        #$this->loadContactSnippet($documentManager);
        // Needed, so that a Document use by loadHomepageGerman is managed.
        $documentManager->flush();
        #$this->updatePages($documentManager, AppFixtures::LOCALE_EN);

        $documentManager->flush();
    }
}
