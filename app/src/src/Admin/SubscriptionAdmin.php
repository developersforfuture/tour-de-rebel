<?php

namespace App\Admin;

use App\Entity\Subscription;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Security\Authorization\PermissionTypes;

class SubscriptionAdmin extends Admin
{

    const SUBSCRIPTION_FORM_KEY = 'subscription_details';
    const SUBSCRIPTION_LIST_VIEW = 'app.subscriptions_list';
    const SUBSCRIPTION_ADD_FORM_VIEW = 'app.subscription_add_form';
    const SUBSCRIPTION_EDIT_FORM_VIEW = 'app.subscription_edit_form';

    /**
     * @var ViewBuilderFactoryInterface
     */
    private $viewBuilderFactory;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $subscriptionNavigationItem = new NavigationItem('app.subscriptions');
        $subscriptionNavigationItem->setView(static::SUBSCRIPTION_LIST_VIEW);
        $subscriptionNavigationItem->setIcon('su-envelope');
        $subscriptionNavigationItem->setPosition(30);

        $navigationItemCollection->add($subscriptionNavigationItem);
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $listView = $this->viewBuilderFactory->createListViewBuilder(static::SUBSCRIPTION_LIST_VIEW, '/subscriptions')
            ->setResourceKey(SUBSCRIPTION::RESOURCE_KEY)
            ->setListKey('subscriptions')
            ->addListAdapters(['table'])
            ->setAddView(static::SUBSCRIPTION_ADD_FORM_VIEW)
            ->setEditView(static::SUBSCRIPTION_EDIT_FORM_VIEW)
            ->addToolbarActions([new ToolbarAction('sulu_admin.add'), new ToolbarAction('sulu_admin.delete')]);

        $viewCollection->add($listView);

        $addFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(static::SUBSCRIPTION_ADD_FORM_VIEW, '/subscriptions/add')
            ->setResourceKey(SUBSCRIPTION::RESOURCE_KEY)
            ->setBackView(static::SUBSCRIPTION_LIST_VIEW);

        $viewCollection->add($addFormView);

        $addDetailsFormView = $this->viewBuilderFactory->createFormViewBuilder(static::SUBSCRIPTION_ADD_FORM_VIEW . '.details', '/details')
            ->setResourceKey(SUBSCRIPTION::RESOURCE_KEY)
            ->setFormKey(static::SUBSCRIPTION_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->setEditView(static::SUBSCRIPTION_EDIT_FORM_VIEW)
            ->addToolbarActions([new ToolbarAction('sulu_admin.save'), new ToolbarAction('sulu_admin.delete')])
            ->setParent(static::SUBSCRIPTION_ADD_FORM_VIEW);

        $viewCollection->add($addDetailsFormView);

        $editFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(static::SUBSCRIPTION_EDIT_FORM_VIEW, '/subscriptions/:id')
            ->setResourceKey(SUBSCRIPTION::RESOURCE_KEY)
            ->setBackView(static::SUBSCRIPTION_LIST_VIEW);

        $viewCollection->add($editFormView);

        $editDetailsFormView = $this->viewBuilderFactory->createFormViewBuilder(static::SUBSCRIPTION_EDIT_FORM_VIEW . '.details', '/details')
            ->setResourceKey(SUBSCRIPTION::RESOURCE_KEY)
            ->setFormKey(static::SUBSCRIPTION_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->addToolbarActions([new ToolbarAction('sulu_admin.save'), new ToolbarAction('sulu_admin.delete')])
            ->setParent(static::SUBSCRIPTION_EDIT_FORM_VIEW);

        $viewCollection->add($editDetailsFormView);
    }

    public function getSecurityContexts()
    {
        return [
            'Sulu' => [
                'TourDePlanet' => [
                    'sulu.tourdeplanet.subscription' => [
                        PermissionTypes::VIEW,
                        PermissionTypes::ADD,
                        PermissionTypes::EDIT,
                        PermissionTypes::DELETE,
                    ],
                ],
            ],
        ];
    }
}
